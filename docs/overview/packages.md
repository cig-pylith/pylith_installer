# Dependency versions

The installer will build the following versions of the packages. Limitations are versions are shown. In some cases we
have patched the libraries to address portability issues.

* Autotools
  * m4 1.4.17
  * autoconf 2.69
  * automake 1.16.1
  * libtool 2.4.6

* Gcc compiler
  * gcc/g++/gfortran 7.3.0
  * mpc 1.0.3
  * gmp 6.1.0
  * mpfr 3.1.4

* MPI (select one)
  * MPICH 3.3.2
  * OpenMPI 4.0.2

* Python
  * Python 2.7.16
  * numpy 1.16.6
  * six 1.14.0
  * cftime 1.1.1
  * setuptools 44.0.0
  * netCDF (Python) 1.5.3
  * h5py 2.10.1

* General tools
  * OpenSSL 1.1.1d
  * CppUnit 1.15.1
  * PCRE 8.40
  * SWIG 3.0.2
  * SQLITE 3310100
  * Proj 6.3.0
  * HDF5 1.10.6
  * NetCDF 4.7.3
  * Cmake 3.16.3

* CIG tools
  * pythia 0.8.1.19
  * nemesis 1.1.0
