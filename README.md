# PyLith Installer

[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/geodynamics/pylith_installer/blob/master/COPYING)


## Description

**IMPORTANT**: If you are just starting to learn how to use PyLith or wanting to run PyLith on a laptop or workstation,
then we highly recommend using the binary packages. Installation instructions for binary packages are available in the
PyLith manual available from the [PyLith webpage](https://geodynamics.org/cig/software/pylith/) on the [CIG
website](https://geodynamics.org).

This installer builds the current PyLith release and its dependencies from source.

## Documentation

See [geodynamics.github.io/pylith_installer](https://geodynamics.github.io/pylith_installer) for instructions on how to
install PyLith using the PyLith Installer utility.å

## Release Notes

See [CHANGES.md](CHANGES.md) for a complete list of changes for each release.

## Author

* Brad Aagaard, Geologic Hazards Science Center, USGS, USA
